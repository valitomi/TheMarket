-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.6.2-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for themarket
CREATE DATABASE IF NOT EXISTS `themarket` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `themarket`;

-- Dumping structure for table themarket.contracts
CREATE TABLE IF NOT EXISTS `contracts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `buyer_user_id` int(11) DEFAULT NULL,
  `seller_user_id` int(11) DEFAULT NULL,
  `item_id` int(11) NOT NULL,
  `price` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `contracts_id_uindex` (`id`),
  KEY `contracts_buyer_users_id_fk` (`buyer_user_id`),
  KEY `contracts_items_fk` (`item_id`),
  KEY `contracts_seller_users__fk` (`seller_user_id`),
  CONSTRAINT `contracts_buyer_users_id_fk` FOREIGN KEY (`buyer_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `contracts_items_fk` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE,
  CONSTRAINT `contracts_seller_users__fk` FOREIGN KEY (`seller_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

-- Dumping data for table themarket.contracts: ~2 rows (approximately)
/*!40000 ALTER TABLE `contracts` DISABLE KEYS */;
INSERT INTO `contracts` (`id`, `buyer_user_id`, `seller_user_id`, `item_id`, `price`) VALUES
	(33, 19, 20, 18, 111),
	(34, 19, 21, 17, 100);
/*!40000 ALTER TABLE `contracts` ENABLE KEYS */;

-- Dumping structure for table themarket.currencies
CREATE TABLE IF NOT EXISTS `currencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `exchange_rates` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Currencies_id_uindex` (`id`),
  UNIQUE KEY `currencies_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table themarket.currencies: ~4 rows (approximately)
/*!40000 ALTER TABLE `currencies` DISABLE KEYS */;
INSERT INTO `currencies` (`id`, `name`, `exchange_rates`) VALUES
	(1, 'EUR', 1),
	(2, 'USD', 1.159911),
	(4, 'BGN', 1.956424),
	(5, 'AED', 4.26047);
/*!40000 ALTER TABLE `currencies` ENABLE KEYS */;

-- Dumping structure for table themarket.items
CREATE TABLE IF NOT EXISTS `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `item_id_uindex` (`id`),
  KEY `item_user_id_fk` (`user_id`),
  CONSTRAINT `item_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- Dumping data for table themarket.items: ~5 rows (approximately)
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` (`id`, `name`, `user_id`) VALUES
	(13, 'E lastic band', 19),
	(15, 'F ork', 20),
	(16, 'B read', 21),
	(17, 'E gg', 19),
	(18, 'T able', 19);
/*!40000 ALTER TABLE `items` ENABLE KEYS */;

-- Dumping structure for table themarket.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `account` double DEFAULT NULL,
  `currency_id` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_uindex` (`id`),
  UNIQUE KEY `user_username_uindex` (`username`),
  KEY `users_currencies_fk` (`currency_id`),
  CONSTRAINT `users_currencies_fk` FOREIGN KEY (`currency_id`) REFERENCES `currencies` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- Dumping data for table themarket.users: ~4 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `account`, `currency_id`) VALUES
	(19, 'Gosho', 27.57297758190066, 1),
	(20, 'Ivan', 200, 1),
	(21, 'Valentin', 400, 2),
	(22, 'string', 200, 1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
