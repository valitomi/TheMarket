package com.example.themarket.services.mappers;

import com.example.themarket.models.Item;
import com.example.themarket.models.User;
import com.example.themarket.models.dtos.ItemDto;
import com.example.themarket.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ItemMapper {

    private final UserService userService;

    @Autowired
    public ItemMapper(UserService userService) {
        this.userService = userService;
    }

    public Item fromDto(ItemDto dto) {
        User getUserFromDto = (userService.getById(dto.getOwnerId()));

        Item item = new Item();
        item.setId(dto.getId());
        item.setName(dto.getName());
        item.setUser(getUserFromDto);

        return item;
    }


}
