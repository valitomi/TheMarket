package com.example.themarket.services;

import com.example.themarket.config.ExternalApiUrlConfig;
import com.example.themarket.models.Currency;
import com.example.themarket.services.interfaces.CurrencyService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;

@Service
public class ExchangeRateManager {

    private final ExternalApiUrlConfig externalApiUrlConfig;
    private final CurrencyService currencyService;

    @Autowired
    public ExchangeRateManager(ExternalApiUrlConfig externalApiUrlConfig,
                               CurrencyService currencyService) {
        this.externalApiUrlConfig = externalApiUrlConfig;
        this.currencyService = currencyService;
    }

    private Map<String, Double> getRates(String forexUrl) throws JsonProcessingException {
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(forexUrl, String.class);

        Map<String, Object> jsonFileAsMap =
                new ObjectMapper().readValue(result, new TypeReference<>() {});

        return (Map<String, Double>) jsonFileAsMap.get("rates");
    }

    //todo activate only when needed!
    //@Scheduled(cron = "*/30 * * * * *")
    private void updateExchangeRates() throws JsonProcessingException {
        Map<String, Double> rates = getRates(externalApiUrlConfig.getForexUrl());
        rates.remove("EUR");
        List<Currency> currencies = currencyService.getAll();

        rates.forEach((key, value) -> currencies.forEach(currency -> {
            if (key.contains(currency.getName())) {
                currency.setExchangeRates(value);
                currencyService.save(currency);
            }
        }));
    }
}
