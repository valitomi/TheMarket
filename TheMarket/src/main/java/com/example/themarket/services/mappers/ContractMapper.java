package com.example.themarket.services.mappers;

import com.example.themarket.exceptions.EntityNotFoundException;
import com.example.themarket.exceptions.InvalidOperationException;
import com.example.themarket.models.Contract;
import com.example.themarket.models.Item;
import com.example.themarket.models.dtos.ContractDto;
import com.example.themarket.services.interfaces.ContractService;
import com.example.themarket.services.interfaces.ItemService;
import com.example.themarket.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ContractMapper {

    private final ItemService itemService;
    private final UserService userService;
    private final ContractService contractService;

    @Autowired
    public ContractMapper(ItemService itemService, UserService userService, ContractService contractService) {
        this.itemService = itemService;
        this.userService = userService;
        this.contractService = contractService;
    }

    public Contract listContractFromDto(ContractDto dto) {
        Item item = itemService.getById(dto.getItemId());

        Contract contract = new Contract();
        contract.setId(dto.getId());
        contract.setSellerUser(item.getUser());
        contract.setItem(item);
        contract.setPrice(dto.getPrice());

        return contract;
    }

    public Contract updateContractFromDto(ContractDto dto) {
        Item item = itemService.getById(dto.getItemId());

        Contract contract = new Contract();
        contract.setId(contractService.getContractByItemId(item.getId()).getId());
        contract.setSellerUser(item.getUser());
        contract.setItem(item);
        contract.setPrice(dto.getPrice());

        return contract;
    }


    public Contract closeContractFromDto(ContractDto dto) {
        Item item = itemService.getById(dto.getItemId());
        Contract contract = contractService.getContractByItemId(item.getId());

        if (contract == null) {
            throw new EntityNotFoundException("Contract is not available!");
        } else if (contract.getBuyerUser() != null) {
            throw new InvalidOperationException("Contract already closed");
        }

        contract.setBuyerUser(userService.getById(dto.getBuyerId()));
        return contract;
    }


}
