package com.example.themarket.services.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BaseService<T, R extends JpaRepository<T,Integer>> {

    List<T> getAll();

    T getById(int id);

    T deleteById(int id);

    T save(T entity);

    T update(T entity);

}
