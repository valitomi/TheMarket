package com.example.themarket.services.interfaces;

import com.example.themarket.models.User;
import com.example.themarket.repositories.UserRepository;

import java.util.List;

public interface UserService extends BaseService<User, UserRepository> {

}
