package com.example.themarket.services.classes;

import com.example.themarket.exceptions.DuplicateEntityException;
import com.example.themarket.exceptions.EntityNotFoundException;
import com.example.themarket.models.User;
import com.example.themarket.repositories.UserRepository;
import com.example.themarket.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends BaseServiceImpl<User, UserRepository> implements UserService {

    private final UserRepository repository;

    @Autowired
    public UserServiceImpl(UserRepository repository) {
        super(repository, User.class);
        this.repository = repository;
    }


    @Override
    public User deleteById(int id) {
        User userToDelete = getById(id);
        repository.deleteById(id);
        return userToDelete;
    }

    @Override
    public User save(User entity) {
        if (getAll().stream().anyMatch(user -> user.getUsername().equals(entity.getUsername()))) {
            throw new DuplicateEntityException("User", "username", entity.getUsername());
        }

        return repository.save(entity);
    }

    @Override
    public User update(User entity) {
        if (getAll().stream().noneMatch(user -> user.getId() == (entity.getId()))) {
            throw new EntityNotFoundException("User", "id", String.valueOf(entity.getId()));
        } else if (getAll().stream().anyMatch(user -> user.getUsername().equals(entity.getUsername()))
                && !getById(entity.getId()).getUsername().equals(entity.getUsername())) {
            throw new DuplicateEntityException("User", "username", entity.getUsername());
        }

        return repository.save(entity);
    }

}
