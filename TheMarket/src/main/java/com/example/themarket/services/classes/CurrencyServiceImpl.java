package com.example.themarket.services.classes;

import com.example.themarket.exceptions.EntityNotFoundException;
import com.example.themarket.models.Contract;
import com.example.themarket.models.Currency;
import com.example.themarket.repositories.ContractRepository;
import com.example.themarket.repositories.CurrencyRepository;
import com.example.themarket.services.interfaces.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CurrencyServiceImpl extends BaseServiceImpl<Currency, CurrencyRepository> implements CurrencyService {

    private final CurrencyRepository repository;

    @Autowired
    public CurrencyServiceImpl(CurrencyRepository repository) {
        super(repository, Currency.class);
        this.repository = repository;
    }

    @Override
    public Currency deleteById(int id) {
        Currency currencyToDelete = getById(id);
        repository.deleteById(id);
        return currencyToDelete;
    }

    @Override
    public Currency save(Currency entity) {
        return repository.save(entity);
    }

    @Override
    public Currency update(Currency entity) {
        if (getAll().stream().noneMatch(item -> item.getId() == (entity.getId()))) {
            throw new EntityNotFoundException("Currency", "id", String.valueOf(entity.getId()));
        }
        return repository.save(entity);
    }

}
