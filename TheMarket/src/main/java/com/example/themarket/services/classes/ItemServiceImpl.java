package com.example.themarket.services.classes;

import com.example.themarket.exceptions.DuplicateEntityException;
import com.example.themarket.exceptions.EntityNotFoundException;
import com.example.themarket.models.Currency;
import com.example.themarket.models.Item;
import com.example.themarket.models.User;
import com.example.themarket.repositories.CurrencyRepository;
import com.example.themarket.repositories.ItemRepository;
import com.example.themarket.repositories.UserRepository;
import com.example.themarket.services.interfaces.ItemService;
import com.example.themarket.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ItemServiceImpl extends BaseServiceImpl<Item, ItemRepository> implements ItemService {

    private final ItemRepository repository;

    @Autowired
    public ItemServiceImpl(ItemRepository repository) {
        super(repository,Item.class);
        this.repository = repository;
    }

    @Override
    public Item deleteById(int id) {
        Item itemToDelete = getById(id);
        repository.deleteById(id);
        return itemToDelete;
    }

    @Override
    public Item save(Item entity) {
        return repository.save(entity);
    }

    @Override
    public Item update(Item entity) {
        if (getAll().stream().noneMatch(item -> item.getId() == (entity.getId()))) {
            throw new EntityNotFoundException("Item", "id", String.valueOf(entity.getId()));
        }
        return repository.save(entity);
    }

    @Override
    public List<Item> getAllItemsByOwnerId(int id) {
        return repository.getAllItemsByOwnerId(id);
    }
}
