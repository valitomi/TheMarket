package com.example.themarket.services.interfaces;

import com.example.themarket.models.Item;
import com.example.themarket.repositories.ItemRepository;

import java.util.List;

public interface ItemService extends BaseService<Item, ItemRepository> {

    List<Item> getAllItemsByOwnerId(int id);

}
