package com.example.themarket.services.interfaces;

import com.example.themarket.exceptions.EntityNotFoundException;
import com.example.themarket.models.Contract;
import com.example.themarket.models.Item;
import com.example.themarket.repositories.ContractRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ContractService extends BaseService<Contract, ContractRepository> {

    List<Contract>getAllContractsBySellerId (int id);

    List<Contract> getAllActiveContracts ();

    List<Contract> getAllClosedContracts ();

    Contract getContractByItemId(int id);

    Contract closeContract(Contract entity);

}
