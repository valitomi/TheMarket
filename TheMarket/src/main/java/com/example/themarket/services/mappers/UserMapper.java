package com.example.themarket.services.mappers;

import com.example.themarket.models.User;
import com.example.themarket.models.Item;
import com.example.themarket.models.dtos.UserDto;
import com.example.themarket.services.interfaces.CurrencyService;
import com.example.themarket.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    private final UserService userService;
    private final CurrencyService currencyService;

    @Autowired
    public UserMapper(UserService userService, CurrencyService currencyService) {
        this.userService = userService;
        this.currencyService = currencyService;
    }

    public User userFromDto(UserDto dto) {
        User user = new User();
        user.setId(dto.getId());
        user.setUsername(dto.getName());
        user.setAccount(dto.getAccount());
        user.setCurrency(currencyService.getById(dto.getCurrencyId()));

        return user;
    }



}
