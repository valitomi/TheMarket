package com.example.themarket.services.interfaces;

import com.example.themarket.models.Currency;
import com.example.themarket.repositories.CurrencyRepository;

import java.util.List;

public interface CurrencyService extends BaseService<Currency, CurrencyRepository> {

}
