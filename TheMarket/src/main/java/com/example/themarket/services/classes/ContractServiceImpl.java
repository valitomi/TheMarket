package com.example.themarket.services.classes;

import com.example.themarket.exceptions.DuplicateEntityException;
import com.example.themarket.exceptions.EntityNotFoundException;
import com.example.themarket.exceptions.InvalidOperationException;
import com.example.themarket.models.Contract;
import com.example.themarket.models.Item;
import com.example.themarket.models.User;
import com.example.themarket.repositories.ContractRepository;
import com.example.themarket.services.interfaces.ContractService;
import com.example.themarket.services.interfaces.ItemService;
import com.example.themarket.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContractServiceImpl extends BaseServiceImpl<Contract, ContractRepository> implements ContractService {

    private final ContractRepository repository;
    private final ItemService itemService;
    private final UserService userService;

    @Autowired
    public ContractServiceImpl(ContractRepository repository, ItemService itemService, UserService userService) {
        super(repository, Contract.class);
        this.repository = repository;
        this.itemService = itemService;
        this.userService = userService;
    }

    @Override
    public Contract deleteById(int id) {
        Contract contractToDelete = getById(id);
        repository.deleteById(id);
        return contractToDelete;
    }

    @Override
    public Contract save(Contract entity) {
        if (isItemListed(entity.getItem())) {
            throw new DuplicateEntityException("Contract", "item id", String.valueOf(entity.getItem().getId()));
        }
        return repository.save(entity);
    }

    @Override
    public Contract update(Contract entity) {
        if (!isItemListed(entity.getItem())) {
            throw new EntityNotFoundException("Contract", "item id", String.valueOf(entity.getItem().getId()));
        }
        contractExists(entity);
        isClosed(entity);

        return repository.save(entity);
    }

    @Override
    public Contract closeContract(Contract entity) {
        if (entity.getBuyerUser() == entity.getSellerUser()) {
            throw new InvalidOperationException("The seller can't buy his own items!");
        } else if (entity.getBuyerUser().getAccount() < entity.getPrice()) {
            throw new InvalidOperationException("A small setback... Your pockets are a bit empty!");
        }
        contractExists(entity);
        transferFunds(entity);

        return repository.save(entity);
    }

    @Override
    public List<Contract> getAllContractsBySellerId(int id) {
        return repository.getAllContractsBySellerId(id);
    }

    @Override
    public List<Contract> getAllActiveContracts() {
        return repository.getAllActiveContracts();
    }

    @Override
    public List<Contract> getAllClosedContracts() {
        return repository.getAllClosedContracts();
    }

    @Override
    public Contract getContractByItemId(int id) {
        if (!isItemListed(itemService.getById(id))) {
            throw new EntityNotFoundException("Contract", "item id", String.valueOf(id));
        }
        return repository.getActiveContractByItemId(id);
    }

    //checks if item is listed in active
    //contract (items from closed contracts
    //can be sold again from the new owner)
    private boolean isItemListed(Item entity) {
        return getAll().stream().filter(contract -> contract.getItem().equals(entity))
                .anyMatch(contract -> contract.getStatus().equals(Contract.ACTIVE_STATUS));
    }

    private void contractExists(Contract entity) {
        if (getAll().stream().noneMatch(contract -> contract.getId() == (entity.getId()))) {
            throw new EntityNotFoundException("Contract", "id", String.valueOf(entity.getId()));
        }
    }

    //checks if the contract is closed
    private void isClosed(Contract entity) {
        if (entity.getBuyerUser() != null) {
            throw new InvalidOperationException("Contract already closed!");
        }
    }

    private void transferFunds(Contract entity) {
        User buyer = entity.getBuyerUser();
        User seller = entity.getSellerUser();
        Item item = entity.getItem();

        double exchangeRate = buyer.getCurrency().getExchangeRates() / seller.getCurrency().getExchangeRates();

        buyer.setAccount(buyer.getAccount() - entity.getPrice() * exchangeRate);
        seller.setAccount(seller.getAccount() + entity.getPrice());
        item.setUser(buyer);

        userService.update(buyer);
        userService.update(seller);
        itemService.update(item);
    }
}
