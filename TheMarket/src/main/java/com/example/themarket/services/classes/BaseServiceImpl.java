package com.example.themarket.services.classes;

import com.example.themarket.exceptions.EntityNotFoundException;
import com.example.themarket.services.interfaces.BaseService;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public abstract class BaseServiceImpl<T, R extends JpaRepository<T, Integer>>
        implements BaseService<T, R> {

    final R genericRepository;
    final String tClass;

    public BaseServiceImpl(R genericRepository, Class<T> tClass) {
        this.genericRepository = genericRepository;
        this.tClass = tClass.getSimpleName();
    }


    @Override
    public List<T> getAll() {
        return genericRepository.findAll();
    }

    @Override
    public T getById(int id) {
        return genericRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(tClass, "id", String.valueOf(id)));
    }


}


