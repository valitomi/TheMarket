package com.example.themarket.exceptions.config;

import com.example.themarket.exceptions.DuplicateEntityException;
import com.example.themarket.exceptions.EntityNotFoundException;
import com.example.themarket.exceptions.InvalidOperationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler(value = {EntityNotFoundException.class})
    public ResponseEntity<Object> handleEntityNotFoundException(EntityNotFoundException e) {
        HttpStatus notFound = HttpStatus.NOT_FOUND;
        ApiException apiException = new ApiException(e.getMessage(), notFound);

        return new ResponseEntity<>(apiException, notFound);
    }

    @ExceptionHandler(value = {DuplicateEntityException.class})
    public ResponseEntity<Object> handleDuplicateEntityException(DuplicateEntityException e) {
        HttpStatus duplicate = HttpStatus.BAD_REQUEST;
        ApiException apiException = new ApiException(e.getMessage(), duplicate);

        return new ResponseEntity<>(apiException, duplicate);
    }

    @ExceptionHandler(value = {InvalidOperationException.class})
    public ResponseEntity<Object> handleInvalidOperationException(InvalidOperationException e) {
        HttpStatus invalid = HttpStatus.BAD_REQUEST;
        ApiException apiException = new ApiException(e.getMessage(), invalid);

        return new ResponseEntity<>(apiException, invalid);
    }

}
