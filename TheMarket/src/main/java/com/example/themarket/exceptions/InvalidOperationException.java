package com.example.themarket.exceptions;

public class InvalidOperationException extends RuntimeException {

    public InvalidOperationException(String type) {
        super(type);
    }


}
