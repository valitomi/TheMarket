package com.example.themarket.exceptions.config;

import org.springframework.http.HttpStatus;

public class ApiException {
    private final String message;
    private final int statusCode;
    private final HttpStatus httpStatus;

    public ApiException(String message,
                        HttpStatus httpStatus) {
        this.message = message;
        statusCode = httpStatus.value();
        this.httpStatus = httpStatus;
    }

    public String getMessage() {
        return message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public int getStatusCode() {
        return statusCode;
    }

}
