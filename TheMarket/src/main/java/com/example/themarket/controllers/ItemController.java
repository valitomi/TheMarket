package com.example.themarket.controllers;


import com.example.themarket.models.Item;
import com.example.themarket.models.dtos.ItemDto;
import com.example.themarket.services.interfaces.ItemService;
import com.example.themarket.services.mappers.ItemMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/items")
public class ItemController {

    private final ItemService service;
    private final ItemMapper mapper;

    @Autowired
    public ItemController(ItemService service, ItemMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping
    public List<Item> getAll() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    public Item getById(@PathVariable int id) {
        return service.getById(id);
    }

    @PostMapping
    public Item create(@Valid @RequestBody ItemDto dto) {
        return service.save(mapper.fromDto(dto));
    }

    @PutMapping("/{id}")
    public Item update(@PathVariable int id, @Valid @RequestBody ItemDto dto) {
        dto.setId(id);
        return service.update(mapper.fromDto(dto));
    }

    @DeleteMapping("/{id}")
    public Item delete(@PathVariable int id) {
        return service.deleteById(id);
    }

    @GetMapping("/searchUserId")
    public List<Item> getAllItemsByOwnerId(@RequestParam int id) {
        return service.getAllItemsByOwnerId(id);
    }

}