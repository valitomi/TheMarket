package com.example.themarket.controllers;


import com.example.themarket.models.User;
import com.example.themarket.models.dtos.UserDto;
import com.example.themarket.services.interfaces.UserService;

import com.example.themarket.services.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService service;
    private final UserMapper userMapper;

    @Autowired
    public UserController(UserService service, UserMapper userMapper) {
        this.service = service;
        this.userMapper = userMapper;
    }

    @GetMapping
    public List<User> getAll() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    public User getById(@PathVariable int id) {
        return service.getById(id);
    }

    @PostMapping
    public User create(@Valid @RequestBody UserDto dto) {
        return service.save(userMapper.userFromDto(dto));
    }

    @PutMapping("/{id}")
    public User update(@PathVariable int id, @Valid @RequestBody UserDto dto) {
        dto.setId(id);
        return service.update(userMapper.userFromDto(dto));
    }

    @DeleteMapping("/{id}")
    public User delete(@PathVariable int id) {
        return service.deleteById(id);
    }

}