package com.example.themarket.controllers;


import com.example.themarket.models.Contract;
import com.example.themarket.models.dtos.ContractDto;
import com.example.themarket.services.interfaces.ContractService;
import com.example.themarket.services.mappers.ContractMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/contracts")
public class ContractController {

    private final ContractService service;
    private final ContractMapper mapper;

    @Autowired
    public ContractController(ContractService service, ContractMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping
    public List<Contract> getAll(@RequestParam(defaultValue = "") String status) {
        if (status.equalsIgnoreCase(Contract.ACTIVE_STATUS)) {
            return service.getAllActiveContracts();
        } else if (status.equalsIgnoreCase(Contract.CLOSED_STATUS)) {
            return service.getAllClosedContracts();
        }

        return service.getAll();
    }

    @GetMapping("/{id}")
    public Contract getById(@PathVariable int id) {
        return service.getById(id);
    }

    @GetMapping("/user/{id}")
    public List<Contract> getBySellerUserId(@PathVariable int id) {
        return service.getAllContractsBySellerId(id);
    }

    @PostMapping
    public Contract create(@Valid @RequestBody ContractDto dto) {
        return service.save(mapper.listContractFromDto(dto));
    }

    @PutMapping("/update-price")
    public Contract updateContract(@Valid @RequestBody ContractDto dto) {
        return service.update(mapper.updateContractFromDto(dto));
    }

    @PutMapping("/update-buyer")
    public Contract closeContract(@Valid @RequestBody ContractDto dto) {
        return service.closeContract(mapper.closeContractFromDto(dto));
    }

    @DeleteMapping("/{id}")
    public Contract delete(@PathVariable int id) {
        return service.deleteById(id);
    }


}