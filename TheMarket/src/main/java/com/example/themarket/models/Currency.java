package com.example.themarket.models;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "currencies", schema = "themarket", catalog = "")
public class Currency {
    private int id;
    private String name;
    private double exchangeRates;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 50)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "exchange_rates", nullable = true, precision = 0)
    public double getExchangeRates() {
        return exchangeRates;
    }

    public void setExchangeRates(double exchangeRates) {
        this.exchangeRates = exchangeRates;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Currency that = (Currency) o;
        return id == that.id &&
                Objects.equals(name, that.name) &&
                Objects.equals(exchangeRates, that.exchangeRates);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, exchangeRates);
    }
}
