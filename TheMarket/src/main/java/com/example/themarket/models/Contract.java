package com.example.themarket.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "contracts", schema = "themarket", catalog = "")
public class Contract {
    public static final String ACTIVE_STATUS = "Active";
    public static final String CLOSED_STATUS = "Closed";

    private int id;
    @JsonIgnore
    private User buyerUser;
    @JsonIgnore
    private User sellerUser;
    @JsonIgnore
    private Item item;
    @JsonIgnore
    private double price;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @ManyToOne
    @JoinColumn(name = "buyer_user_id", referencedColumnName = "id")
    public User getBuyerUser() {
        return buyerUser;
    }

    public void setBuyerUser(User buyerUser) {
        this.buyerUser = buyerUser;
    }

    @ManyToOne
    @JoinColumn(name = "seller_user_id", referencedColumnName = "id")
    public User getSellerUser() {
        return sellerUser;
    }

    public void setSellerUser(User sellerUser) {
        this.sellerUser = sellerUser;
    }

    @ManyToOne
    @JoinColumn(name = "item_id", referencedColumnName = "id", nullable = false)
    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    @Basic
    @Column(name = "price", nullable = true)
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Transient
    public int getSellerId() {
        return sellerUser.getId();
    }

    @Transient
    public String getSellerName() {
        return sellerUser.getUsername();
    }

    @Transient
    public String getStatus() {
        return buyerUser == null ? ACTIVE_STATUS : CLOSED_STATUS;
    }

    @Transient
    public int getItemId() {
        return item.getId();
    }

    @Transient
    public String getBuyerId() {
        return buyerUser == null ? "No one" : String.valueOf(sellerUser.getId());
    }

    @Transient
    public String getPriceTotal() {
        return String.format("%.2f %s", price, sellerUser.getCurrency().getName());
    }


}
