package com.example.themarket.repositories;

import com.example.themarket.models.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemRepository extends JpaRepository<Item, Integer> {

    @Query(value = "select * from themarket.items where user_id = :id",nativeQuery = true)
    List<Item> getAllItemsByOwnerId(int id);

}