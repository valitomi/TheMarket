package com.example.themarket.repositories;

import com.example.themarket.models.Contract;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContractRepository extends JpaRepository<Contract, Integer> {

    @Query(value = "select * from themarket.contracts where seller_user_id = :id", nativeQuery = true)
    List<Contract> getAllContractsBySellerId(int id);

    @Query(value = "select * from themarket.contracts where buyer_user_id is  null", nativeQuery = true)
    List<Contract> getAllActiveContracts();

    @Query(value = "select * from themarket.contracts where buyer_user_id is not null", nativeQuery = true)
    List<Contract> getAllClosedContracts();

    @Query(value = "select * from themarket.contracts where contracts.item_id =:id and buyer_user_id is null", nativeQuery = true)
    Contract getActiveContractByItemId(int id);

}