package com.example.themarket.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("classpath:application.properties")
public class ExternalApiUrlConfig {


    private final String forexUrl;

    public ExternalApiUrlConfig(Environment environment) {
        this.forexUrl = environment.getProperty("forex.url");
    }

    public String getForexUrl() {
        return forexUrl;
    }
}
