package com.example.themarket.services;

import com.example.themarket.Helpers;
import com.example.themarket.exceptions.EntityNotFoundException;
import com.example.themarket.repositories.UserRepository;
import com.example.themarket.services.classes.UserServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class UserServiceTests {


    @Mock
    UserRepository mockRepository;

    @InjectMocks
    UserServiceImpl service;

    @Test
    public void getAll_should_callRepository() {
        Mockito.when(mockRepository.findAll())
                .thenReturn(new ArrayList<>());

        service.getAll();

        Mockito.verify(mockRepository, Mockito.times(1)).findAll();
    }


    @Test
    public void getById_should_throwException_when_matchDoesntExist() {
        int nonExistingId = 12345;

        Assertions.assertThrows(EntityNotFoundException.class, () -> service.getById(nonExistingId));
    }

    @Test
    public void deleteById_should_throwException_when_matchDoesntExist() {
        int nonExistingId = 12345;

        Assertions.assertThrows(EntityNotFoundException.class, () -> service.deleteById(nonExistingId));
    }

    @Test
    public void findById() {
        var expected = Helpers.createMockUser1();
        Mockito.when(mockRepository.findById(expected.getId()))
                .thenReturn(Optional.of(expected));
        var actual = service.getById(expected.getId());

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void createUser() {
        var expected = Helpers.createMockUser1();
        Mockito.when(mockRepository.save(expected))
                .thenReturn(expected);
        var actual = service.save(expected);

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void updateUser_should_throwException_when_matchDoesntExist() {
        var user = Helpers.createMockUser1();


        Assertions.assertThrows(EntityNotFoundException.class, () -> service.update(user));
    }


}
