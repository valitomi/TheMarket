package com.example.themarket.services;

import com.example.themarket.exceptions.EntityNotFoundException;
import com.example.themarket.repositories.ItemRepository;
import com.example.themarket.services.classes.ItemServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

@ExtendWith(MockitoExtension.class)
public class ItemServiceTests {
    @Mock
    ItemRepository mockRepository;

    @InjectMocks
    ItemServiceImpl service;

    @Test
    public void getAll_should_callRepository() {
        Mockito.when(mockRepository.findAll())
                .thenReturn(new ArrayList<>());

        service.getAll();

        Mockito.verify(mockRepository, Mockito.times(1)).findAll();
    }



    @Test
    public void getById_should_throwException_when_matchDoesntExist() {
        int nonExistingId = 12345;

        Assertions.assertThrows(EntityNotFoundException.class, () -> service.getById(nonExistingId));
    }

    @Test
    public void deleteById_should_throwException_when_matchDoesntExist() {
        int nonExistingId = 12345;

        Assertions.assertThrows(EntityNotFoundException.class, () -> service.deleteById( nonExistingId));
    }




}
