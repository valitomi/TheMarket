package com.example.themarket.services;

import com.example.themarket.Helpers;
import com.example.themarket.exceptions.EntityNotFoundException;
import com.example.themarket.repositories.CurrencyRepository;
import com.example.themarket.repositories.UserRepository;
import com.example.themarket.services.classes.CurrencyServiceImpl;
import com.example.themarket.services.classes.UserServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

@ExtendWith(MockitoExtension.class)
public class CurrencyServiceTests {
    @Mock
    CurrencyRepository mockRepository;

    @InjectMocks
    CurrencyServiceImpl service;

    @Test
    public void getAll_should_callRepository() {

        mockRepository.save(Helpers.createMockCurrency());

        Mockito.when(mockRepository.findAll())
                .thenReturn(new ArrayList<>());

        System.out.println(mockRepository.findAll());

        Mockito.verify(mockRepository, Mockito.times(1)).findAll();
    }


    @Test
    public void getById_should_throwException_when_matchDoesntExist() {
        int nonExistingId = 12345;

        Assertions.assertThrows(EntityNotFoundException.class, () -> service.getById(nonExistingId));
    }

    @Test
    public void deleteById_should_throwException_when_matchDoesntExist() {
        int nonExistingId = 12345;

        Assertions.assertThrows(EntityNotFoundException.class, () -> service.deleteById(nonExistingId));
    }




}
