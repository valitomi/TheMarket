package com.example.themarket.services;

import com.example.themarket.Helpers;
import com.example.themarket.exceptions.DuplicateEntityException;
import com.example.themarket.exceptions.EntityNotFoundException;
import com.example.themarket.models.Contract;
import com.example.themarket.repositories.ContractRepository;
import com.example.themarket.services.classes.ContractServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class ContractServiceTests {
    @Mock
    ContractRepository mockRepository;

    @InjectMocks
    ContractServiceImpl service;

    @Test
    public void getAll_should_callRepository() {
        Mockito.when(mockRepository.findAll())
                .thenReturn(new ArrayList<>());

        service.getAll();

        Mockito.verify(mockRepository, Mockito.times(1)).findAll();
    }



    @Test
    public void getById_should_throwException_when_matchDoesntExist() {
        int nonExistingId = 12345;

        Assertions.assertThrows(EntityNotFoundException.class, () -> service.getById(nonExistingId));
    }

    @Test
    public void deleteById_should_throwException_when_matchDoesntExist() {
        int nonExistingId = 12345;

        Assertions.assertThrows(EntityNotFoundException.class, () -> service.deleteById( nonExistingId));
    }




}
