package com.example.themarket;

import com.example.themarket.models.Contract;
import com.example.themarket.models.Currency;
import com.example.themarket.models.Item;
import com.example.themarket.models.User;


public class Helpers {

    public static Contract createMockContract() {
        return new Contract(1, null, createMockUser2(), createMockItem(), 50);
    }

    public static Currency createMockCurrency() {
        return new Currency(1, "BGN", 2);
    }

    public static User createMockUser1() {
        return new User(1, "MockUser1", 1000, createMockCurrency());
    }

    public static User createMockUser2() {
        return new User(2, "MockUser2", 0, createMockCurrency());
    }

    public static Item createMockItem() {
        return new Item(1, "MockItem", createMockUser1());
    }

}
