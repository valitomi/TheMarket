**The project has Swagger documentation which can be accessed by the following link:**


http://localhost:8080/swagger-ui/index.html#/

In the DB_scripts folder you can find database scripts and create the database for this project. It's relational database constructed with MariaDB

![](./Pictures/DB_Picture.png)
 

**Supported functionalities and handled conflicts:**

 

_**All entities can be created,updated, deleted(on delete cascade added to every table in the DB), viewed by id or viewed all:**_

-Creating more than one **Entity** with the same name throws DuplicateEntityException

-Updating an non-existent **Entity** throws EntityNotFoundException

-Get by id throws EntityNotFoundException if there is no **Entity** with such id

 

_**Users**_

-Updating a **User** and assigning a name that is being used by another **User** throws DuplicateEntityException, but not when the pre-updated **User** has the same name


 


_**Items can be viewed by owner(User) Id**_


 

_**Contracts can be viewed by status(Active/Closed), by seller(User) id**_

-Creating more than one **Contract** with the same **Item** throws DuplicateEntityException, does not include closed **Contracts**

   (If an **Item** is in a closed **Contract**, the **Item** can be listed in a new **Contract** by the new owner(**User**))

-Updating a closed **Contracts** throws InvalidOperationException

-Closing a **Contract** throws InvalidOperationException if the buyer(**User**) is the same **User** who listed the **Contract**

-Closing a **Contract** throws InvalidOperationException if the buyer(**User**) does not have enough money

-**Contract**'s **Currency** is the same as the **User**'s **Currency** who listed it

 

_**All Currency rates have EUR as base**_
 
 

_**Instructions for activating the Currency Api (In ExchangeRateManager class):**_

 ![](./Pictures/ActivateCurrencyApi.png)

_**Small bonus on the index page :)**_

http://localhost:8080/








